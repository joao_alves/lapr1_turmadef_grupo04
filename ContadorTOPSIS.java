/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr1_turmaf_grupo04projeto;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import static lapr1_turmaf_grupo04projeto.LerTOPSSIS.isNumber;

/**
 *
 * @author João Alves
 */
public class ContadorTOPSIS {

    public static int[] contadorTop(File ficheiro) throws FileNotFoundException {
        int[] tamanho = new int[4]; // posicao 0 representa a quantidade de criterios, posicao 1 quantidade de alternativas, posicao 2 o numero de criterios beneficios e 3 o numero de criterios custo
        for (int i = 0; i < tamanho.length; i++) {
            tamanho[i] = 0;

        }

        if (ficheiro.exists()) {
            Scanner fxLeitura = new Scanner(ficheiro);

            while (fxLeitura.hasNext()) {

                String linha[] = fxLeitura.nextLine().split(" +");

                if (linha[0].equalsIgnoreCase("crt_beneficio")) {

                    tamanho[2] = linha.length - 1;

                }// colocar os beneficios na string vetor benefi

                if (linha[0].equalsIgnoreCase("crt_custo")) {
                    tamanho[3] = linha.length - 1;
                }// colocar os custos na string vetor custo

                if (linha[0].equalsIgnoreCase("md_crt_alt")) {
                    linha = fxLeitura.nextLine().split(" +");
                    if (linha[0].equalsIgnoreCase("crt")) {

                        tamanho[0] = linha.length - 1;
                        linha = fxLeitura.nextLine().split(" +");
                    } // colocar os criterios na variavel criterios

                    if (linha[0].equalsIgnoreCase("alt")) {

                        tamanho[1] = linha.length - 1;

                    } //colocar alternativas na variavel alternativas

                }

            }
        }
        return tamanho;

    }

}
