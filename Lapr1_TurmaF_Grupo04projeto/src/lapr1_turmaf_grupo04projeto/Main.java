package lapr1_turmaf_grupo04projeto;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
/**
 *
 * @author João Alves e Beatriz
 */
public class Main
{

    private final static int CRI = 3;
    private final static int ALT = 4;

    
    public static void main(String[] args) throws FileNotFoundException {

        double[] indice;
        indice = new double[]{0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.38, 1.41, 
        1.45, 1.49, 1.51, 1.48, 1.56, 1.57, 1.59}; // vetor indices
        int op;
        
        String[][] cab = new String[4][5];              //cabeçalhos  
        double[][] mc_criterios = new double[CRI][CRI];      // matriz dos criterios       
        double[][] mc_1 = new double[ALT][ALT];    // matriz do 1º critério
        double[][] mc_2 = new double[ALT][ALT];   // matriz do 2º critério
        double[][] mc_3 = new double[ALT][ALT];  // matriz do 3º critério

        File file = openFile(); //Abre o ficheiro se o mesmo existir

        Ler.lerFicheiro(file, cab, mc_criterios, mc_1, mc_2, mc_3); //Lê o ficheiro e organiza-o em matrizes
        
        do {
            op = Menu.choose();
            switch (op) {
                case 0:

                    Aproximado.metodoAHP(mc_criterios, mc_1, mc_2, mc_3, CRI, indice);
                    op = 9;
                    break;
                case 1:

                    Exato.exato(mc_criterios, mc_1, mc_2, mc_3, indice);
                    op = 9;
                    break;
                
                case 9:
                    System.out.println("Adeus!");
                    break;
                                      
                                       
                default:
                    System.out.println("Introduza uma opção válida:");
                    break;
            }
        } while (op!=9); // Opção 9 sai do programa

    }

    public static File openFile() {
        Scanner ler = new Scanner(System.in);   
        System.out.println("Introduza o nome do ficheiro ou out para sair:");
        File file = new File(ler.nextLine());
        if ("out".equals(file.getName())){
                System.exit(0); 
            }
        if (!file.exists()) {
            System.out.println("\nFicheiro não encontrado!\n");
            openFile();  //senão existir ficheiro abrir outro
            
            
           
        }
        return file;
    }

}
