package lapr1_turmaf_grupo04projeto;

import java.io.FileNotFoundException;
import static lapr1_turmaf_grupo04projeto.Utilitarios.prioridadeComp;
import static lapr1_turmaf_grupo04projeto.Utilitarios.ic;
import static lapr1_turmaf_grupo04projeto.Utilitarios.rc;
import static lapr1_turmaf_grupo04projeto.Utilitarios.verifica;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 *
 * @author João Alves e Beatriz
 */
public class Exato
{

    private final static int CRI = 3;
    private final static int ALT = 4;

    public static void exato(double[][] mc_criterios, double[][] mc_1, double[][] mc_2, double[][] mc_3, double[] indice) throws FileNotFoundException
    {
        int v;
        double[] vp_cri;
        double[] vp_1;
        double[] vp_2;
        double[] vp_3;
        double[] vResultado;
        double[] ic = new double[ALT];
        double[] rc = new double[ALT];
        double[] mv = new double[ALT];
        double[][] empty = new double[0][0];

        vp_cri = eigen(mc_criterios, mv, CRI, 0);   //criação do vetor proprio dos criterios
        vp_1 = eigen(mc_1, mv, ALT, 1); //criação do vetor proprio do 1º criterio
        vp_2 = eigen(mc_2, mv, ALT, 2); //criação do vetor proprio do 2º criterio
        vp_3 = eigen(mc_3, mv, ALT, 3); //criação do vetor proprio do 3º criterio

        ic[0] = ic(mv[0], CRI); //calculo do ic dos criterios
        ic[1] = ic(mv[1], ALT); //calculo do ic do 1º criterio
        ic[2] = ic(mv[2], ALT); //calculo do ic do 2º criterio
        ic[3] = ic(mv[3], ALT); //calculo do ic do 3º criterio

        rc[0] = rc(ic[0], indice, CRI); //calculo do rc dos criterios
        rc[1] = rc(ic[1], indice, ALT); //calculo do rc do 1º criterio
        rc[2] = rc(ic[2], indice, ALT); //calculo do rc do 2º criterio
        rc[3] = rc(ic[3], indice, ALT); //calculo do rc do 3º criterio

        vResultado = prioridadeComp(ALT, vp_cri, vp_1, vp_2, vp_3); // calculo do vetor resultado

        v = verifica(ic);   // verificação dos ic´s (Se existir algum maior que 0.2 o prgrama não corre)

        if (v == 1)
        {

            Escrever.escreverResultado(mc_criterios, mc_1, mc_2, mc_3, vResultado, empty, empty, empty, empty, ic[0], rc[0], ic[1], rc[1], ic[2], rc[2], ic[3], rc[3], mv[0], mv[1], mv[2], mv[3], vp_cri, vp_1, vp_2, vp_3);
        } else
        {
            System.out.print("Existem valores de IC maiores que 0.2!");
        }
    }

        public static double[] eigen(double[][] m, double[] vMax, int dim, int pos) {
        double[] vpri = new double[dim];

        Matrix a = new Basic2DMatrix(m);
        EigenDecompositor eigenD = new EigenDecompositor(a);
        Matrix[] mattD = eigenD.decompose();

        double matA[][] = mattD[0].toDenseMatrix().toArray();
        double matB[][] = mattD[1].toDenseMatrix().toArray();

        vMax[pos] = Math.abs(matB[0][0]); //Valor maximo absoluto

        for (int j = 0; j < dim; j++) {
            vpri[j] = Math.abs(matA[j][0]);
        }

        return vpri;
    }
}

