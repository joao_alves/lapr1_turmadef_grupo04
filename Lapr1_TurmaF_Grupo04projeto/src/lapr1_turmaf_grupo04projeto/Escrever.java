package lapr1_turmaf_grupo04projeto;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;

/**
 *
 * @author Beatriz
 */
public class Escrever
{

    private final static int CRI = 3;
    private final static int ALT = 4;
    
//Escreve para a consola, cria e passa para o ficheiro resultado.txt

    public static void escreverResultado(double[][] mc_criterios, double[][] mc_1,
            double[][] mc_2, double[][] mc_3,
            double[] vResultado, double[][] nmc_cri, double[][] nmc_1,
            double[][] nmc_2, double[][] nmc_3,
            double ic_cri, double rc_cri, double ic_1, double rc_1, double ic_2, double rc_2,
            double ic_3, double rc_3, double mv_cri, double mv_1,
            double mv_2, double mv_3, double[] vp_cri, double[] vp_1, double[] vp_2, double[] vp_3) throws FileNotFoundException
    {
    //cria o ficheiro resultado.txt
        File ficheiro = new File("Resultado.txt");
        try (Formatter write = new Formatter(ficheiro))
        {
            //mostrar matrizes normalizadas 
            //matriz critérios normalizada
            if (nmc_cri.length > 0)
            {
                write.format("%s%n", "Matriz Criterio Normalizada:");   //para guardar no ficheiro   
                for (int i = 0; i < CRI; i++)
                {
                    for (int j = 0; j < CRI; j++)
                    {
                        write.format("%.2f ", (nmc_cri[i][j]));
                    }
                    write.format("%n");
                }
            }
            if (nmc_cri.length == 0)
            {
                write.format("CRITÉRIOS:");
            }
            write.format("%n");
            write.format("IC: %.2f ", ic_cri); // mostra IC
            write.format("%n");
            write.format("RC: %.2f ", rc_cri);  // mostra RC
            write.format("%n");
            write.format("Maior Valor Proprio: %.2f ", mv_cri); // mostra maior valor proprio
            write.format("%n");
            write.format("%n%s%n", "Vetor Prioridades:");
            for (int i = 0; i < vp_cri.length; i++)  // percorre o vetor prioridades e guarda-o no ficheiro  
            {
                write.format("%.2f%n", vp_cri[i]);
            }
            write.format("%n");
            write.format("%n");

            if (nmc_1.length > 0)
            {
                write.format("%s%n", "Matriz Estilo Normalizada:");   // percorre a matriz estilo e escreve-a no ficheiro 
                for (int i = 0; i < ALT; i++)
                {
                    for (int j = 0; j < ALT; j++)
                    {
                        write.format("%.2f ", nmc_1[i][j]);
                    }
                    write.format("%n");
                }
            }
            if (nmc_1.length == 0)
            {
                write.format("ESTILO:");
            }
            write.format("%n", "");
            write.format("IC: %.2f ", ic_1); // mostra o IC do estilo
            write.format("%n");
            write.format("RC: %.2f ", rc_1);  //// mostra o RC do estilo
            write.format("%n");
            write.format("Maior Valor Proprio: %.2f ", mv_1);  // mostra o maior valor proprio do estilo
            write.format("%n");
            write.format("%n%s%n", "Vetor Prioridades:"); // percorre o vetor prioridades e guarda-o no ficheiro   
            for (int i = 0; i < vp_1.length; i++)
            {
                write.format("%.2f%n", vp_1[i]);
            }
            write.format("%n");
            write.format("%n");

            if (nmc_2.length > 0)
            {
                write.format("%s%n", "Matriz Confiabilidade Normalizada:");   // percorre a matriz confiabilidade e escreve-a no ficheiro   
                for (int i = 0; i < ALT; i++)
                {
                    for (int j = 0; j < ALT; j++)
                    {
                        write.format("%.2f ", nmc_2[i][j]);
                    }
                    write.format("%n");
                }
            }
            if (nmc_2.length == 0)
            {
                write.format("CONFIABILIDADE:");
            }
            write.format("%n");
            write.format("IC: %.2f ", ic_2);  // mostra o IC do  confiabilidade
            write.format("%n");
            write.format("RC: %.2f ", rc_2);  // mostra o RC do confiabilidade
            write.format("%n");
            write.format("Maior Valor Proprio: %.2f ", mv_2);   // mostra o maior valor proprio da confiabilidade
            write.format("%n");
            write.format("%n%s%n", "Vetor Prioridades:");  // percorre a matriz confiabilidade e escreve-a no ficheiro 
            for (int i = 0; i < vp_2.length; i++)
            {
                write.format("%.2f%n", vp_2[i]);
            }
            write.format("%n");
            write.format("%n");

            if (nmc_3.length > 0)
            {
                write.format("%s%n", "Matriz Consumo Normalizada:");   // percorre a matriz consumo e escreve-a no ficheiro 
                for (int i = 0; i < ALT; i++)
                {
                    for (int j = 0; j < ALT; j++)
                    {
                        write.format("%.2f ", nmc_3[i][j]);
                    }
                    write.format("%n");
                }
            }
            if (nmc_3.length == 0)
            {
                write.format("CONSUMO:");
            }
            write.format("%n");
            write.format("IC: %.2f ", ic_3);  // mostra o IC do  consumo
            write.format("%n");
            write.format("RC: %.2f ", rc_3);  // mostra o RC do consumo
            write.format("%n");
            write.format("Maior Valor Proprio: %.2f ", mv_3); // mostra o maior valor proprio da consumo
            write.format("%n");
            write.format("%n%s%n", "Vetor Prioridades:");   // percorre a matriz consumoe e escreve-a no ficheiro 
            for (int i = 0; i < vp_3.length; i++)
            {
                write.format("%.2f%n", vp_3[i]);
            }
            write.format("%n");
            write.format("%n");
            write.format("%n");

            //mostra matrizes normalizadas juntamente com vetor de prioridades
            write.format("%n");

            //mostrar as matrizes de comparacao de entrada
            System.out.printf("%s%n", "Matriz Criterios:"); //para imprimir no ecra
            write.format("%s%n", "Matriz Criterios:");   //para guardar no ficheiro   
            for (int i = 0; i < CRI; i++)
            {
                for (int j = 0; j < CRI; j++)
                {
                    System.out.printf("%.2f ", (mc_criterios[i][j]));
                    write.format("%.2f ", (mc_criterios[i][j]));
                }
                System.out.printf("%n");
                write.format("%n");
            }
            write.format("%n");

            System.out.printf("%n%s%n", "Estilo:"); //para imprimir no ecra
            write.format("%n%s%n", "Estilo:");   //para guardar no ficheiro   
            for (int i = 0; i < ALT; i++)
            {
                for (int j = 0; j < ALT; j++)
                {
                    System.out.printf("%.2f ", (mc_1[i][j]));
                    write.format("%.2f ", (mc_1[i][j]));
                }
                System.out.printf("%n");
                write.format("%n");
            }
            write.format("%n");

            System.out.printf("%n%s%n", "Confiabilidade:"); //para imprimir no ecrÃ£
            write.format("%n%s%n", "Confiabilidade:");   //para guardar no ficheiro   
            for (int i = 0; i < ALT; i++)
            {
                for (int j = 0; j < ALT; j++)
                {
                    System.out.printf("%.2f ", (mc_2[i][j]));
                    write.format("%.2f ", (mc_2[i][j]));
                }
                System.out.printf("%n");
                write.format("%n");
            }
            write.format("%n");

            System.out.printf("%n%s%n", "Consumo:"); //para imprimir no ecra
            write.format("%n%s%n", "Consumo:");   //para guardar no ficheiro   
            for (int i = 0; i < ALT; i++)
            {
                for (int j = 0; j < ALT; j++)
                {

                    System.out.printf("%.2f ", (mc_3[i][j]));
                    write.format("%.2f ", (mc_3[i][j]));
                }
                System.out.printf("%n");
                write.format("%n");
            }
            write.format("%n");

            //para mostra vetor de prioridade composta
            System.out.printf("%n%s%n", "Vetor de Prioridade Composta:"); //para imprimir no ecra
            write.format("%n%s%n", "Vetor de Prioridade Composta:");  //para guardar no ficheiro 
            for (int i = 0; i < vResultado.length; i++)
            {
                System.out.printf("%.2f%n", vResultado[i]);
                write.format("%.2f%n", vResultado[i]);
            }
            write.format("%n");

            //para mostrar a melhor alternativa
            double maior = 0;

            for (int i = 0; i < vResultado.length; i++)
            {
                if (vResultado[i] > maior)
                {
                    maior = vResultado[i];
                }
            }
            if (maior == vResultado[0])
            {
                System.out.printf("%n%s", "A melhor alternativa consoante as suas preferencias e o car1.");
                System.out.println("");
                write.format("%n%s", "A melhor alternativa consoante as suas preferencias e o car1.");
            }
            if (maior == vResultado[1])
            {
                System.out.printf("%n%s", "A melhor alternativa consoante as preferencias e o car2.");
                System.out.println("");
                write.format("%n%s", "A melhor alternativa consoante as suas preferencias e o car2.");
            }
            if (maior == vResultado[2])
            {
                System.out.printf("%n%s", "A melhor alternativa consoante as suas preferencias e o car3.");
                System.out.println("");
                write.format("%n%s", "A melhor alternativa consoante as suas preferencias e o car3.");
            }
            if (maior == vResultado[3])
            {
                System.out.printf("%n%s", "A melhor alternativa consoante as suas preferencias e o car4.");
                System.out.println("");
                write.format("%n%s", "A melhor alternativa consoante as suas preferencias e o car4.");
            }
        }

    }
}
