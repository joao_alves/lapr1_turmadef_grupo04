package lapr1_turmaf_grupo04projeto;

import java.io.FileNotFoundException;
import static lapr1_turmaf_grupo04projeto.Utilitarios.prioridadeComp;
import static lapr1_turmaf_grupo04projeto.Utilitarios.ic;
import static lapr1_turmaf_grupo04projeto.Utilitarios.rc;
import static lapr1_turmaf_grupo04projeto.Utilitarios.verifica;


/**
 *
 * @author Beatriz
 */
public class Aproximado
{

    private final static int CRI = 3;
    private final static int ALT = 4;

    public static void metodoAHP(double[][] mc_criterios, double[][] mc_1, double[][] mc_2, double[][] mc_3, int dim, double[] Indice) throws FileNotFoundException
    {

        int v;
        double[] vp_cri;
        double[] vp_1;
        double[] vp_2;
        double[] vp_3;
        double[] vResultado;
        double[][] nmc_cri;
        double[][] nmc_1;
        double[][] nmc_2;
        double[][] nmc_3;
        double[] ic = new double[ALT];
        double[] rc = new double[ALT];
        double[] mv = new double[ALT];

        nmc_cri = normalize(mc_criterios, CRI); //normalização da matriz criterios
        nmc_1 = normalize(mc_1, ALT);   //normalização da matriz do 1º criterio
        nmc_2 = normalize(mc_2, ALT);   //normalização da matriz do 2º criterio   
        nmc_3 = normalize(mc_3, ALT);   //normalização da matriz do 3º criterio

        vp_cri = vetorProprio(nmc_cri, CRI);    //criação do vetor proprio dos criterios
        vp_1 = vetorProprio(nmc_1, ALT);    //criação do vetor proprio do 1º criterio
        vp_2 = vetorProprio(nmc_2, ALT);    //criação do vetor proprio do 2º criterio
        vp_3 = vetorProprio(nmc_3, ALT);    //criação do vetor proprio do 3º criterio

        mv[0] = mValor(mc_criterios, vp_cri, CRI);    //calculo do maior valor proprio dos criterios
        mv[1] = mValor(mc_1, vp_1, ALT);  //calculo do maior valor proprio do 1º criterio
        mv[2] = mValor(mc_2, vp_2, ALT);  //calculo do maior valor proprio do 2º criterio
        mv[3] = mValor(mc_3, vp_3, ALT);  //calculo do maior valor proprio do 3º criterio

        ic[0] = ic(mv[0], CRI); //calculo do ic dos criterios
        ic[1] = ic(mv[1], ALT); //calculo do ic do 1º criterio
        ic[2] = ic(mv[2], ALT); //calculo do ic do 2º criterio
        ic[3] = ic(mv[3], ALT); //calculo do ic do 3º criterio

        rc[0] = rc(ic[0], Indice, CRI); //calculo do rc dos criterios
        rc[1] = rc(ic[1], Indice, ALT); //calculo do rc do 1º criterio
        rc[2] = rc(ic[2], Indice, ALT); //calculo do rc do 2º criterio
        rc[3] = rc(ic[3], Indice, ALT); //calculo do rc do 3º criterio

        vResultado = prioridadeComp(ALT, vp_cri, vp_1, vp_2, vp_3); // calculo do vetor resultado

        v = verifica(ic); // verificação dos ic´s (Se existir algum maior que 0.2 o prgrama não corre)

        if (v == 1)
        {

            Escrever.escreverResultado(mc_criterios, mc_1, mc_2, mc_3, vResultado, nmc_cri, nmc_1, nmc_2, nmc_3, ic[0], rc[0], ic[1], rc[1], ic[2], rc[2], ic[3], rc[3], mv[0], mv[1], mv[2], mv[3], vp_cri, vp_1, vp_2, vp_3);
        } else
        {
            System.out.print("Existem valores de IR maiores que 0.2!");
        }

    }
/**
 * Metodo utilizado para normalizar matrizes
 * @param m - matriz em questão
 * @param dim - dimensão da matriz (nº de linhas sempre igual ao nº de colunas)
 * @return Matriz normalizada.
 */
    public static double[][] normalize(double[][] m, int dim)
    {

        double[][] res = new double[dim][dim];
        double[] soma_c = new double[dim];

        for (int i = 0; i < dim; i++)
        {

            for (int p = 0; p < dim; p++)
            {
                soma_c[i] = soma_c[i] + m[p][i];
            }
        }//somatorio das colunas feito

        for (int i = 0; i < dim; i++)
        {

            for (int p = 0; p < dim; p++)
            {
                res[p][i] = (m[p][i] / soma_c[i]);
            }

        }//Divisão da matriz pelo somatorio das colunas

        return res;
    }

    /**
     * Este Metodo e utilizado para calcular vetores proprios
     * @param m - Matriz que prentendemos utilizar.
     * @param dim - Dimensão da matriz em questão.
     * @return Vetor proprio da matriz de entrada.
     */
    public static double[] vetorProprio(double[][] m, int dim)
    {

        double[] vpro = new double[dim];

        for (int i = 0; i < dim; i++)
        {

            for (int p = 0; p < dim; p++)
            {
                vpro[i] = vpro[i] + m[i][p];
            }

        }

        for (int i = 0; i < dim; i++)
        {

            vpro[i] = vpro[i] / dim;

        }

        return vpro;
    }

    /**
     * Este metodo calcula o maior valor proprio.
     * @param m - Matriz de entrada.
     * @param vp - Vetor Proprio da matriz em questao.
     * @param dim - Dimensao da matriz e do vetor proprio.
     * @return maior valor proprio
     */
    public static double mValor(double[][] m, double vp[], int dim)
    {
        double res = 0;
        
        double[] multi = new double[dim];
        double[] div = new double[dim];

        for (int i = 0; i < dim; i++)
        {

            for (int p = 0; p < dim; p++)
            {
                multi[i] = multi[i] + (m[i][p] * vp[p]);
            }

        } //Multiplicação

        for (int i = 0; i < dim; i++)
        {

            div[i] = multi[i] / vp[i];

        }//Divisão

        for (int i = 0; i < dim; i++)
        {

            res = res + div[i];

        }

        res = res / dim;

        return res;
    }

   
}
