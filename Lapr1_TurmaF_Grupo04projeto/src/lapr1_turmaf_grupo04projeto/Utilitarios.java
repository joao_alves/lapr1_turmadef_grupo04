package lapr1_turmaf_grupo04projeto;

/**
 *
 * @author João Alves
 */
public class Utilitarios 
{
    private final static int CRI = 3;
    private final static int ALT = 4;
    /**
     * Metodo utilizado para calcular o ic
     * @param max - Maior valor absoluto
     * @param dim - nº de criterios
     * @return o ic
     */
        public static double ic(double max, int dim)
    {

        return (max - dim) / (dim - 1);
    }

    /**
     *  Metodo utilizado para calcular o rc.
     * @param ic - ic para o qual queremos obter o rc
     * @param indice - Indices;
     * @param dim - nº de criterios
     * @return rc.
     */    
    public static double rc(double ic, double[] indice, int dim)
    {

        return ic / indice[dim - 1];

    }
/**
 * 
 * @param dim - dimensao dos vetores prioridade (tem de ser todos iguais)
 * @param prio - vetor prioridade dos criterios.
 * @param pro1  - vetor prioridade do 1º criterio.
 * @param pro2  - vetor prioridade do 2º criterio.
 * @param pro3  - vetor prioridade do 3º criterio.
 * @return - vetor prioridade completo
 */
    public static double[] prioridadeComp(int dim, double[] prio, double[] pro1, double[] pro2, double[] pro3)
    {
        double[][] matrixPri = new double[CRI][dim];
        double[] priComp = new double[dim];

        for (int i = 0; i < dim; i++)
        {

            for (int p = 0; p < dim; p++)
            {
                if (i == 0)
                {
                    matrixPri[i][p] = pro1[p];
                }
                if (i == 1)
                {
                    matrixPri[i][p] = pro2[p];
                }
                if (i == 2)
                {
                    matrixPri[i][p] = pro3[p];
                }
            }
        }//organizaçõ da Matrix Prioridades

        for (int i = 0; i < ALT; i++)
        {

            for (int p = 0; p < CRI; p++)
            {
                priComp[i] = priComp[i] + (matrixPri[p][i] * prio[p]);
            }

        }//Vetor Composto

        return priComp;
    }
/**
 * Metodo usado para verificar se os ic´s estão dentro do limite de 0.2
 * 
 * @param v - vetor que contem os ic´s
 * 
 * @return 0 se algum ic for superior 0.2 ou 1 se oc ic´s estiverem abaixo de 0.2
 */
    public static int verifica(double[] v)
    {
        int res = 1, p = 0;

        for (int i = 0; i < ALT; i++)
        {
            if (v[i] > 0.2)
            {
                res = 0;
            }

        }

        return res;

    }
}
