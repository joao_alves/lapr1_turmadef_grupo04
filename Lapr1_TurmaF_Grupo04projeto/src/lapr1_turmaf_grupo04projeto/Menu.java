
package lapr1_turmaf_grupo04projeto;

import java.util.Scanner;

/**
 *
 * @author João Alves e Beatriz
 */
public class Menu
{
       public static int choose() {
        Scanner in = new Scanner(System.in);
        int op; 
        String texto = "_________________________________________________________________________________\n"
                + "                                       MENU                                      \n"
                + "_________________________________________________________________________________\n\n"
                + "Método aproximado.............................................................(0)\n"
                + "Método exato..................................................................(1)\n"
                + "_________________________________________________________________________________\n"
                + "_________________________________________________________________________________\n"
                + "SAIR.........................................................................(9)\n"
                + "_________________________________________________________________________________\n"
                + "Introduza uma opção:";

        System.out.printf("%n%s%n", texto);
        String aux = in.nextLine();
        if (isNumber(aux)) {
            op = Integer.parseInt(aux);
        } else {
            return -2;
        }

        return op;
    }
 
    public static boolean isNumber(String str) { //Verifica se é um numero positivo
        for (int i = 0, n = str.length(); i < n; i++) {
            int ascii = (int) str.charAt(i);
            if (ascii < 48 || ascii > 57) {
                return false;
            }
        }
        return true;
    }

}
