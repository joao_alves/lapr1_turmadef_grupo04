package lapr1_turmaf_grupo04projeto;

import java.io.File;
import java.io.FileNotFoundException;
import static java.lang.System.in;
import java.text.ParseException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * @author Beatriz
 */
public class Topsis
{

    public static void lerTopsis(String nomeFx) throws FileNotFoundException, ParseException
    {
        Scanner fxLeitura = new Scanner(new File(nomeFx));

        String linha;
        while (fxLeitura.hasNext())
        {
            linha = fxLeitura.nextLine().trim();
            if (linha.length() > 0) //linhas em branco
            {
                String[] beneficios = linha.split(" ");  // divide a linha pelos espaços
                if (beneficios[0].equalsIgnoreCase("crt _beneficios"))
                {
                    int nbeneficios = beneficios.length - 1;
                    for (int i = 0; i < nbeneficios; i++)
                    {
                        beneficios[i] = beneficios[i + 1].trim().toUpperCase();
                    }
                }
                String[] custos = linha.split(" ");
                if (custos[0].equalsIgnoreCase("crt _custos"))
                {
                    int ncustos = custos.length - 1;
                    for (int i = 0; i < ncustos; i++)
                    {
                        custos[i] = custos[i + 1].trim().toUpperCase();
                    }
                }

                String[] spesos = linha.split(" ");
                if (spesos[0].equalsIgnoreCase("vec_pesos"))
                {
                    String[] pesos = fxLeitura.nextLine().split(" ");
                    Double[] vpesos = new Double[pesos.length];
                    for (int i = 0; i < pesos.length; i++)
                    {
                        pesos[i] = pesos[i].trim();
                        vpesos[i] = Double.parseDouble(pesos[i]); // vetor que contem os pesos
                    }
                }

                String[] smatriz = linha.split(" ");
                if (smatriz[0].equalsIgnoreCase("md_crt_alt"))
                {
                    String[] cri = fxLeitura.nextLine().split(" ");
                    if (cri[0].equalsIgnoreCase("crt"))
                    {
                        String[] criterios = fxLeitura.nextLine().split(" ");
                        for (int i = 0; i < criterios.length - 1; i++)
                        {
                            criterios[i] = criterios[i + 1].trim().toUpperCase();
                        }
                    }
                    
                    String[] alt = fxLeitura.nextLine().split(" ");
                    if (alt[0].equalsIgnoreCase("alt"))
                    {
                        String[] alternativas = fxLeitura.nextLine().split(" ");
                        for (int i = 0; i < alternativas.length - 1; i++)
                        {
                            alternativas[i] = alternativas[i + 1].trim().toUpperCase();
                            int nalternativas = alternativas.length;
                          
                          
                             //guardar a matriz (???)
                        }
                    }


                }
            }
        }
    }
        
        /**
         * Metodo usado para construir a matriz de decisao normalizada
         *
         * @param matDecisao - matriz com m alternativas e n criterios deﬁnidos
         * pelo utilizador
         *
         * @return - retorna a matriz decisao normalizada .
         */
    public static double[][] normaliz(double[][] matDecisao)
    {
        double[][] matNormaliz = new double[matDecisao.length][matDecisao[0].length];
        double[] somaColuna = new double[matDecisao[0].length];

        for (int i = 0; i < matDecisao.length; i++)
        {
            for (int p = 0; p < matDecisao[0].length; p++)
            {
                somaColuna[i] += Math.pow(matDecisao[p][i], 2);
                // matriz decisao elevado a 2 e soma das colunas
            }
            somaColuna[i] = Math.pow(somaColuna[i], 0.5);
        }//somatorio das colunas feito

        for (int i = 0; i < matDecisao.length; i++)
        {
            for (int p = 0; p < matDecisao[0].length; p++)
            {
                matNormaliz[p][i] = (matDecisao[p][i] / somaColuna[i]);
                //Divisão da matriz pelo somatorio das colunas
            }
        }
        return matNormaliz;
    }

    /**
     * Metodo usado para construir a matriz de decisao normalizada pesada
     *
     * @param matNormaliz - matriz normalizada
     * @param vpesos - vetor peso de cada criterio
     * @return - retorna a matriz pesada normalizada
     *
     */
    public static double[][] normalizPesada(double[] vpesos, double[][] matNormaliz)
    {
        normaliz(matNormaliz);
        double[][] matNormalizPesada = new double[matNormaliz.length][matNormaliz[0].length];

        for (int i = 0; i < matNormaliz.length; i++)
        {
            for (int j = 0; j < matNormaliz[0].length; j++)
            {
                matNormalizPesada[i][j] = vpesos[j] * matNormaliz[i][j];
                // multiplicacao do vetor pesos pela matriz normalizada
            }
        }
        return matNormalizPesada;
    }

    /**
     * Metodo usado para guardar os maiores valores , a soluçao ideal de cada
     * criterio
     *
     * @param matNormalizPesada - martriz normalizada pesada
     *
     * @param ncribeneficios - numero de criterios beneficios
     *
     * @return - retorna o vetor com as solucoes ideais
     *
     */
    public static double[] solucaoIdeal(double[][] matNormalizPesada, int ncribeneficios)
    {
        double[] solucaoideal = new double[matNormalizPesada[0].length];

        for (int j = 0; j < ncribeneficios; j++)
        {
            solucaoideal[j] = 0;
            for (int i = 0; i < matNormalizPesada.length; i++)
            {
                if (solucaoideal[j] < matNormalizPesada[i][j])
                {
                    solucaoideal[j] = matNormalizPesada[i][j];
                }

            }
        }

        for (int j = ncribeneficios; j < matNormalizPesada.length; j++)
        {
            solucaoideal[j] = 1;
            for (int i = 0; i < matNormalizPesada.length; i++)
            {
                if (matNormalizPesada[i][j] < solucaoideal[j])
                {
                    solucaoideal[j] = matNormalizPesada[i][j];
                }

            }
        }
        return solucaoideal;
    }

    /**
     * Metodo usado para guardar os menores valores , a soluçao ideal negativa
     * de cada criterio
     *
     * @param matNormalizPesada
     *
     * @param ncribeneficios - numero de criterios beneficios
     *
     * @return - retorna o vetor com as solucoes ideais negativas
     *
     */
    public static double[] solucaoIdealNeg(double[][] matNormalizPesada, int ncribeneficios)
    {
        double[] solucaoidealNeg = new double[matNormalizPesada[0].length];
        for (int j = 0; j < ncribeneficios; j++)
        {
            solucaoidealNeg[j] = 1;
            for (int i = 0; i < matNormalizPesada.length; i++)
            {
                if (matNormalizPesada[i][j] < solucaoidealNeg[j])
                {
                    solucaoidealNeg[j] = matNormalizPesada[i][j];
                }

            }
        }
        for (int j = ncribeneficios; j < matNormalizPesada.length; j++)
        {
            solucaoidealNeg[j] = 0;
            for (int i = 0; i < matNormalizPesada.length; i++)
            {
                if (matNormalizPesada[i][j] > solucaoidealNeg[j])
                {
                    solucaoidealNeg[j] = matNormalizPesada[i][j];
                }

            }
        }
        return solucaoidealNeg;

    }
// Para fazer o neg é igual mas muda o parametro de entrada solucaoideal - solucaoidealNeg

    /**
     * metodo que calcula a distancia a solucao ideal
     *
     * @param matNormalizPesada
     * @param solucaoideal
     * @return distanciaAlt - distancia das alternativas a solucao ideal
     */
    public static double[] distanciaSolucaoIdeal(double[][] matNormalizPesada, double[] solucaoideal)
    {
        double[] distanciaAlt = new double[matNormalizPesada[0].length];
        double[] operacoes = new double[matNormalizPesada[0].length];

        for (int i = 0; i < matNormalizPesada.length; i++)
        {
            for (int p = 0; p < matNormalizPesada[0].length; p++)
            {
                operacoes[i] += Math.pow((matNormalizPesada[i][p] - solucaoideal[p]), 2);
                ////somatorio das colunas da matriz e subtraçao ao quadrado do valor da solucao ideal ( distancia de cada alternativa a solucao ideal
            }
            distanciaAlt[i] = Math.pow(operacoes[i], 0.5);
        }//elevado a 1/2
        return distanciaAlt;
    }

    /**
     * metodo que calcula a proximidade relativa a solucao ideal
     *
     * @param distanciaAlt
     * @param distanciaAltNeg
     *
     * @return proximidade - vetor de proximidade relativa
     */
    public static double[] proxrelativaSolIdeal(double[] distanciaAlt, double[] distanciaAltNeg)
    {
        double[] proximidade = new double[distanciaAlt.length];
        for (int i = 0; i < distanciaAlt.length; i++)
        {
            proximidade[i] = distanciaAltNeg[i] / (distanciaAlt[i] + distanciaAltNeg[i]);
        }
        return proximidade;
    }

    /**
     * metodo que serve para descobrir qual a alternativa escolhida
     *
     * @param proximidade
     * @param alternativas
     * @return altEscolhida - retorna a string que é a alt escolhida
     */
    public static String alternativaEscolhida(double[] proximidade, String[] alternativas)
    {
        // Ordena vetor e escolhe o que for maior

        int pos = maiorValorArray(proximidade); //  posição da melhor alternativa
        String altEscolhida = alternativas[pos];
        return altEscolhida;
    }

    /**
     * procura a posiçao do maior valor do vetor proximidade
     *
     * @param proximidade
     * @return posMaiorValor - posiçao do maior valor
     */
    public static int maiorValorArray(double[] proximidade)
    {

        double maiorValor = 0;
        int posMaiorValor = 0;

        for (int i = 0; i < proximidade.length; i++)
        {

            if (proximidade[i] > maiorValor)
            {
                maiorValor = proximidade[i];
                posMaiorValor = i;
            }
        }

        return posMaiorValor;
    }

    /**
     * Escrever para ficheiro e consola
     *
     * @param vpesos
     * @param matNormaliz
     * @param matDecisao
     * @param matNormalizPesada
     * @param solucaoideal
     * @param solucaoidealNeg
     * @param distanciaAltNeg
     * @param distanciaAlt
     * @param proximidade
     * @param cribeneficios
     * @param cricustos
     * @param alternativaescolhida
     * @param criterios
     * @param alternativas
     * @param CRI
     * @param ALT
     * @throws FileNotFoundException
     */
    public static void escrever(double[] vpesos, double[][] matNormaliz, double[][] matDecisao,
            double[][] matNormalizPesada, double[] solucaoideal, double[] solucaoidealNeg,
            double[] distanciaAltNeg, double[] distanciaAlt, double[] proximidade,
            String[] cribeneficios, String[] cricustos, String alternativaescolhida,
            String[] criterios, String[] alternativas,
            int CRI, int ALT) throws FileNotFoundException
    {
        File ficheiro = new File("Resultado.txt");
        try (Formatter write = new Formatter(ficheiro))
        {
            // dados de entrada
            write.format("%s%n", "Criterios beneficios:");  //para guardar no ficheiro   
            System.out.printf("%s%n", "Criterios beneficios:");   //para mostrar na consola
            for (int i = 0; i < cribeneficios.length; i++)
            {
                write.format("%s%n", cribeneficios[i]);
                System.out.printf("%s%n", cribeneficios[i]);
            }
            write.format("%n");
            System.out.printf("%n");

            write.format("%s%n", "Criterios custos:");  //para guardar no ficheiro    
            System.out.printf("%s%n", "Criterios custos:");//para mostrar na consola
            {
                for (int i = 0; i < cricustos.length; i++)
                {
                    write.format("%s%n", cricustos[i]);
                    System.out.printf("%s%n", cricustos[i]);
                }
            }
            write.format("%n");
            System.out.printf("%n");

            write.format("%s%n", "Vetor pesos:");   //para guardar no ficheiro   
            System.out.printf("%s%n", "Vetor pesos:"); //para mostrar na consola
            for (int i = 0; i < vpesos.length; i++)
            {
                write.format("%.2f%n", vpesos[i]);
                System.out.printf("%.2f%n", vpesos[i]); //para mostrar na consola
            }
            write.format("%n");
            System.out.printf("%n");

            write.format("%s%n", "Criterios:");  //para guardar no ficheiro   
            System.out.printf("%s%n", "Criterios:");   //para mostrar na consola
            for (int i = 0; i < criterios.length; i++)
            {
                write.format("%s%n", criterios[i]);
                System.out.printf("%s%n", criterios[i]);
            }
            write.format("%n");
            System.out.printf("%n");

            write.format("%s%n", "Alternativas:");  //para guardar no ficheiro   
            System.out.printf("%s%n", "Alternativas:");   //para mostrar na consola
            for (int i = 0; i < alternativas.length; i++)
            {

                write.format("%s%n", alternativas[i]);
                System.out.printf("%s%n", alternativas[i]);
            }
            write.format("%n");
            System.out.printf("%n");

            write.format("%s%n", "Matriz Decisao (entrada):");   //para guardar no ficheiro   
            System.out.printf("%s%n", "Matriz Decisao (entrada):");   //para guardar na consola
            for (int i = 0; i < CRI; i++)
            {
                for (int j = 0; j < ALT; j++)
                {
                    write.format("%.2f ", (matDecisao[i][j]));
                    System.out.printf("%.2f ", (matDecisao[i][j]));  //para mostrar na consola
                }
                write.format("%n");
                System.out.printf("%n");
            }
            write.format("%n");
            System.out.printf("%n");

            // fim dos dados de entrada
            write.format("%s%n", "Matriz Normalizada:");   //para guardar no ficheiro   
            for (int i = 0; i < CRI; i++)
            {
                for (int j = 0; j < ALT; j++)
                {
                    write.format("%.2f ", (matNormaliz[i][j]));
                }
                write.format("%n");

            }
            write.format("%n");

            write.format("%s%n", "Matriz Normalizada Pesada:");   //para guardar no ficheiro 
            System.out.printf("%s%n", "Matriz Normalizada Pesada:"); //para mostrar na consola
            for (int i = 0; i < CRI; i++)
            {
                for (int j = 0; j < ALT; j++)
                {
                    write.format("%.2f ", (matNormalizPesada[i][j]));
                    System.out.printf("%.2f", matNormalizPesada[i][j]);   //para mostrar na consola
                }
                write.format("%n");
                System.out.printf("%n");
            }
            write.format("%n");
            System.out.printf("%n");

            write.format("%s%n", "Vetor Solução ideal:");   //para guardar no ficheiro   
            for (int i = 0; i < solucaoideal.length; i++)
            {
                write.format("%.2f ", (solucaoideal[i]));
            }
            write.format("%n");

            write.format("%n%s%n", "Vetor Solução ideal negativa:");   //para guardar no ficheiro   
            for (int i = 0; i < solucaoidealNeg.length; i++)
            {
                write.format("%.2f ", (solucaoidealNeg[i]));
            }
            write.format("%n");

            write.format("%n%s%n", "Vetor distâcia entre cada alternativa e a soluçao ideal:");   //para guardar no ficheiro   
            for (int i = 0; i < distanciaAlt.length; i++)
            {
                write.format("%.2f ", (distanciaAlt[i]));
            }
            write.format("%n");

            write.format("%n%s%n", "Vetor distâcia entre cada alternativa e a soluçao ideal negativa:");   //para guardar no ficheiro   
            for (int i = 0; i < distanciaAltNeg.length; i++)
            {
                write.format("%.2f ", (distanciaAltNeg[i]));
            }
            write.format("%n");

            write.format("%n%s%n", "Vetor proximidade relativa à solução ideal");   //para guardar no ficheiro   
            System.out.printf("%s%n", "Vetor proximidade relativa à solução ideal:"); //para mostrar na consola
            for (int i = 0; i < proximidade.length; i++)
            {
                write.format("%.2f ", (proximidade[i]));
                System.out.printf("%s%n", proximidade[i]);   //para mostrar na consola
            }
            write.format("%n");

            write.format("%n%s%n", "Alternativa escolhida:" + alternativaescolhida);   //para guardar no ficheiro   
            System.out.printf("%n%s%n", "Alternativa escolhida:" + alternativaescolhida);   //para mostrar na consola

        }
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) throws FileNotFoundException
    {
        double[][] matDecisao =
        {
            {
                7, 9, 9, 8
            },
            {
                8, 7, 8, 7
            },
            {
                9, 6, 8, 9
            },
            {
                6, 7, 8, 6
            }
        };
        int ncribeneficios = 3;
        int ncricustos = 1;
        int CRI = 4;
        int ALT = 4;
        String[] beneficios =
        {
            "Estilo", "Confiabilidade", "Consumo"
        };
        String[] custos =
        {
            "Custo"
        };

        String[] criterios =
        {
            "Estilo", "Confiabilidade", "Consumo", "Custo"
        };
        String[] alternativas =
        {
            "Civic", "Saturn", "Ford", "Mazda"
        };
        double[] vpesos =
        {
            0.1, 0.4, 0.3, 0.2
        };
        double[][] matNormaliz = normaliz(matDecisao);
        double[][] matNormalizPesada = normalizPesada(vpesos, matNormaliz);
        double[] solIdeal = solucaoIdeal(matNormalizPesada, ncribeneficios);
        double[] solIdealNeg = solucaoIdealNeg(matNormalizPesada, ncribeneficios);
        double[] distsolIdeal = distanciaSolucaoIdeal(matNormalizPesada, solIdeal);
        // Para fazer o neg é igual mas muda o parametro de entrada solucaoideal - solucaoidealNeg
        double[] distsolIdealNeg = distanciaSolucaoIdeal(matNormalizPesada, solIdealNeg);
        double[] proximidade = proxrelativaSolIdeal(distsolIdeal, distsolIdealNeg);
        String alternativaescolhida = alternativaEscolhida(proximidade, alternativas);
        escrever(vpesos, matNormaliz, matDecisao,
                matNormalizPesada, solIdeal, solIdealNeg,
                distsolIdeal, distsolIdealNeg, proximidade,
                beneficios, custos, alternativaescolhida,
                criterios, alternativas,
                CRI, ALT);
    }
}
