
package lapr1_turmaf_grupo04projeto;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Joao
 */
public class Ler
{
     public static int lerFicheiro(File ficheiro, String cab[][], double mc_criterios[][], double mc_1[][], double mc_2[][], double mc_3[][]) throws FileNotFoundException {

        if (ficheiro.exists()) {
            Scanner fxLeitura = new Scanner(ficheiro);
            int bloco = -1;
            int n_linha = -1;
            while (fxLeitura.hasNext()) {
                String[] linha = fxLeitura.nextLine().split(" +");
                if (linha.length > 1) {
                    if (n_linha == -1) {
                        bloco++;
                        System.arraycopy(linha, 0, cab[bloco], 0, linha.length);
                        n_linha++;
                    } else {
                        switch (bloco) {
                            case 0:
                                tratamento(linha, mc_criterios, n_linha);
                                n_linha++;
                                break;
                            case 1:
                                tratamento(linha, mc_1, n_linha);
                                n_linha++;
                                break;
                            case 2:
                                tratamento(linha, mc_2, n_linha);
                                n_linha++;
                                break;
                            case 3:
                                tratamento(linha, mc_3, n_linha);
                                n_linha++;
                                break;
                        }
                    }
                } else {
                    n_linha = -1;
                }
            }
            return 1;
        } else {
            return 0;
        }
    }
         public static void tratamento(String[] leitura, double[][] mAtual, int n_linha) {
        for (int i = 0; i < leitura.length; i++) {
            try { //tentar ver se é double, se for colocar na matriz
                mAtual[n_linha][i] = Double.parseDouble(leitura[i]);
            } catch (NumberFormatException e) {
                String[] fra = leitura[i].split("/");
                mAtual[n_linha][i] = (Double.parseDouble(fra[0])) / (Double.parseDouble(fra[1]));
            }
        }
    }

}
