package lapr1_turmaf_grupo04projeto;

import java.io.File;
import java.io.FileNotFoundException;
import static java.nio.file.Files.size;
import java.util.Scanner;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author João Alves
 */
public class LerTOPSSIS {

    public static void lerFicheiroTop(File ficheiro, String benefi[], String[] custo, String[] criterios, String[] alternativas, double[] pesos, double[][] matrizDeci, int nCri) throws FileNotFoundException {
        int sinalizador = 0;

        if (ficheiro.exists()) {
            Scanner fxLeitura = new Scanner(ficheiro);

            while (fxLeitura.hasNext()) {

                String linha[] = fxLeitura.nextLine().split(" +");
                if (linha.length >= 0) {

                    if (linha[0].equalsIgnoreCase("crt_beneficio")) {
                        for (int i = 0; i < linha.length - 1; i++) {
                            benefi[i] = linha[i + 1];

                        }

                    }// colocar os beneficios na string vetor benefi

                    if (linha[0].equalsIgnoreCase("crt_custo")) {
                        for (int i = 0; i < linha.length - 1; i++) {
                            custo[i] = linha[i + 1];

                        }

                    }// colocar os custos na string vetor custo

                    if (linha[0].equalsIgnoreCase("vec_pesos")) {
                        linha = fxLeitura.nextLine().split(" +");
                        for (int i = 0; i < linha.length; i++) {

                            pesos[i] = Double.parseDouble(linha[i]);
                            sinalizador = 1;
                        }

                    }// colocar os pesos no vetor double pesos

                    if (linha[0].equalsIgnoreCase("md_crt_alt")) {
                        linha = fxLeitura.nextLine().split(" +");
                        if (linha[0].equalsIgnoreCase("crt")) {
                            for (int i = 0; i < linha.length - 1; i++) {
                                criterios[i] = linha[i + 1];

                            }

                            linha = fxLeitura.nextLine().split(" +");
                        } // colocar os criterios na variavel criterios
                        if (linha[0].equalsIgnoreCase("alt")) {
                            for (int i = 0; i < linha.length - 1; i++) {
                                alternativas[i] = linha[i + 1];

                            }

                        } //colocar alternativas na variavel alternativas
                        linha = fxLeitura.nextLine().split(" +");
                        if (isNumber(linha[0])) {
                            for (int i = 0; i < linha.length; i++) {
                                for (int j = 0; j < linha.length; j++) {
                                    matrizDeci[i][j] = Double.parseDouble(linha[j]);

                                }
                                if (i < linha.length - 1) {
                                    linha = fxLeitura.nextLine().split(" +");
                                }
                            }
                        }//colocar matriz na variavel matrizDeci
                    }

                }
            }

        }
        if (sinalizador == 0) {
            for (int i = 0; i < nCri; i++) {
                pesos[i] = 1 / nCri;
            }
        } // Se não exitirem pesos no ficheiro de entrada ficam todos com o mesmo peso

    }

    public static boolean isNumber(String str) { //Verifica se é um numero positivo
        for (int i = 0, n = str.length(); i < n; i++) {
            int ascii = (int) str.charAt(i);
            if (ascii < 48 || ascii > 57) {
                return false;
            }
        }
        return true;
    }
}
